---
title: Setup for IBM Cloud Account
subtitle: How to setup your IBM Cloud account for our workshop
comments: false
---

In order to complete the IBM Cloud workshops you'll need
the ability to provision a Kubernetes Cluster. This is a detailed walk
through of that process.

<style>
    img {
        border: 2px #445588 solid;
    }
    section {
        width: 800px;
    }
    #banner {
        width: 100%;
    }
    nav {
        text-align: left;
    }
    li.tag-h3 {
        padding-left: 8px;
    }
</style>

## Step 1: Register for IBM Cloud

Go to [https://cloud.ibm.com/][vcpi] to start the registration.
<!-- HAVE YOU HIT THE BACON BUTTON YET? https://console.bluemix.net/registration/whitelist -->

![registration page](images/regpage.png)

Fill out all the require information and the CAPTCHA and
submit. You'll get a confirmation page like:

![confirmation page](images/confirmemail.png)

## Step 2: Confirm Email

Check your email and confirm your account. This will take you to an
IBM coders site. You can ignore this.

## Step 3: IBM Cloud Console

Navigate to the [Dashboard](https://console.bluemix.net/dashboard/apps/)

It will ask you to login:

**NOTE**: Your _IBMid_ is your email address.

![login screen](images/login.png)

## Step 4: Get a Kubernetes Cluster

### Grant Yourself a Cluster

Navigate to the [Grant Clusters Page][grantclusters]

Enter the Lab Key given by your instructor, as well as the email address
associated with your IBM Cloud account you just created. The Region may be
be given to you by your instructor, if you don't have the option don't
worry about it. After you hit submit, you will be given a
cluster for the duration of the workshop.

![grant clusters](images/grant_clusters.png)

(Did something go wrong? Raise your hand, someone will come find you, and [click here](https://ibm.gitlab.io/workshop/cloud/promocodes) we may need to do this instead.)

## Step 5: Cloud Shell

### Connect to your cluster using the Cloud Shell

For this lab, a hosted shell is provided for you with all the necessary tools. Use this web shell to perform the tasks in this lab.

1. Using Chrome or Firefox, go to the [Cloud Shell][cloudshell] and login using the Login button.
1. Passcode is `kubelol`
1. Using the account drop down, choose the 'IBM' account (provided during the Step 4).
1. Click on the Terminal icon to launch your web shell.

![](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-LSpiket623cSUxTA6Ov%2F-LT-VQjiCaEasjeVfeUc%2F-LSz6xEgq2SYZD3kLkxE%2Fcloudshell.png?generation=1544049247213839&alt=media)


## Step 6: Access your cluster

Learn how to set the context to work with your cluster by using the kubectl CLI, access the Kubernetes dashboard, and gather basic information about your cluster.

Set the context for your cluster in your CLI. Every time you log in to the IBM Cloud Kubernetes Service CLI to work with the cluster, you must run these commands to set the path to the cluster's configuration file as a session variable. The Kubernetes CLI uses this variable to find a local configuration file and certificates that are necessary to connect with the cluster in IBM Cloud.

Go back to your [cloud shell][cloudshell]

a. List the available clusters.
```
ibmcloud ks clusters
```
**Note:** If no clusters are shown, make sure you are targeting the right region with `ibmcloud ks region-set`.

b. Set an environment variable for your cluster name:
```
export MYCLUSTER=<your_cluster_name>
```
c. Download the configuration file and certificates for your cluster using the cluster-config command.
```
ibmcloud ks cluster-config $MYCLUSTER
```
c. Copy and paste the output export command from the previous step to set the `KUBECONFIG` environment variable and configure your CLI to run kubectl commands against your cluster. Example: `export KUBECONFIG=/Users...`

Get basic information about your cluster and its worker nodes. This information can help you manage your cluster and troubleshoot issues.

a. View details of your cluster.
```
ibmcloud ks cluster-get $MYCLUSTER
```
b. Verify the worker nodes in the cluster.
```
ibmcloud ks workers $MYCLUSTER
ibmcloud ks worker-get <worker_ID>
```
Validate access to your cluster.

a. View nodes in the cluster.
```
kubectl get node
```
b. View services, deployments, and pods.
```
kubectl get svc,deploy,po --all-namespaces
```

**NOTE:** If the cloud shell isn't working you can use [this](https://ibm.gitlab.io/workshop/cloud/localinstallation) to attempt to install to your local machine.

## Step 7: Continue to the Workshop

Now you are ready to run the [workshops](https://ibm.gitlab.io/workshop/)

[cloudshell]: https://ibm.biz/kube101-cloudshell
[grantclusters]: https://devopsdaysindy.mybluemix.net/
[vcpi]: https://ibm.biz/Bd2kTj
