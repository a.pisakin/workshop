---
title: Kube 201 Workshop
subtitle: The Kubernetes 201 worshop steps
comments: false
---

This is the workflow for the Kubernetes 201 Workshop.

## Learning Objectives

During this workshop you'll do the following things

- [Create some useful shortcuts for using Kubernetes on a daily basis](#pimp)
- [Walk through a typical workflow locally and deploy remotely](#localk8sdev)
- [Using namespaces to deploy to Dev and Prod](#namespaces)
- [Creating an Ingress rule](#ingress)

## Introduction

Congratulations on getting through the Kube 101 workshop. No joke that's a huge feat, and if you feel
comfortable, believe it or not, you have a foundational knowledge of kubernetes now. You are dangerous
in the cloud native space, and you should pat yourself on the back.

If you here, and reading this, this Kube 201 workshop, is designed for a handful more advanced and
quality of life improvements that, as a proficient kubernetes user, you should take into consideration.
Have fun and hopefully you'll learn something new, and if you have some questions reach out we're here
to help.

<a name="pimp"></a>
## Creating some useful aliases etc.
<a name="pimp"></a>

This has been inspired from [Eric Liu's Medium Post](https://itnext.io/pimp-my-kubernetes-shell-f144710232a0) thank you for what you do for the community.

First thing first, if you haven't done this yet, you should do this alias in your shell.

```bash
alias k=kubectl
```

Add it to your `.bashrc` or whatever `$SHELL` you use. It will save you an
unbelievable amount of time.

Second, if you have multiple kubernetes clusters, running that massive export every time is tedious. Instead of running the following, change it from:

```
$ ibmcloud ks cluster-config $your_cluster_name
OK
The configuration for test-cluster was downloaded successfully.

Export environment variables to start using Kubernetes.

export KUBECONFIG=/home/$you/.bluemix/plugins/container-service/clusters/$your_cluster_name/kube-config-dal10-$your_cluster_name.yml
$ export KUBECONFIG=/home/$you/.bluemix/plugins/container-service/clusters/$your_cluster_name/kube-config-dal10-$your_cluster_name.yml
```

To:

```bash
eval "$(ibmcloud ks cluster-config --export $your_cluster_name)"
```

Or one step farther:

```bash
your_cluster_name() {
 eval "$(ibmcloud ks cluster-config --export $your_cluster_name)"
}
```

You can add this as an alias or script, so you can switch back and forth as long as you are logged into your `ibmcloud` account.

Third, there is a project called [kube-ps1](https://github.com/jonmosco/kube-ps1) and as you can see from the following gif, it makes your command prompt tell you which one you are logged into at that moment.

![](https://raw.githubusercontent.com/jonmosco/kube-ps1/master/img/kube-ps1.gif)
If you have more then a couple kubernetes clusters this is well worth the time and effort to set up.

Fourth, there is a project called [kubectx](https://github.com/ahmetb/kubectx) which helps switch between clusters and namespaces. As you can see from the following gif, it makes moving around your multiple namespaces much easier. This could come in handy for later on in this workshop.

![](https://github.com/ahmetb/kubectx/raw/master/img/kubens-demo.gif)

<a name="localk8sdev"></a>
## Local k8s development with a remote deployment
<a name="localk8sdev"></a>

As a developer you want to be able to rapidly iterate on your application source code locally while still mirroring a remote production environment as closely as possible.

### Download and Build the Sample Application

The application shown in this tutorial is a simple guestbook website where users can post messages. You should clone it to your workstation since you will be building it locally.

```bash
git clone https://github.com/IBM/guestbook.git
```

For the purposes of this tutorial the application is run without a backing database (i.e. data is stored in-memory).

Let's go ahead and build the guestbook application.

```bash
$ cd guestbook/v1/guestbook
$ docker build -t guestbook:v1 .
Sending build context to Docker daemon   16.9kB
Step 1/13 : FROM golang as builder
latest: Pulling from library/golang
05d1a5232b46: Pull complete
5cee356eda6b: Pull complete
89d3385f0fd3: Pull complete
80ae6b477848: Pull complete
94ebfeaaddf3: Pull complete
dd697213ec59: Pull complete
715d1281dfe7: Pull complete
Digest: sha256:e8e4c4406217b415c506815d38e3f8ac6e05d0121b19f686c5af7eaadf96f081
Status: Downloaded newer image for golang:latest
 ---> fb7a47d8605b
Step 2/13 : RUN go get github.com/codegangsta/negroni
 ---> Running in 6867267a6832
Removing intermediate container 6867267a6832
 ---> 41cdd0ea4dee
Step 3/13 : RUN go get github.com/gorilla/mux github.com/xyproto/simpleredis
 ---> Running in 6f928ff5ec97
Removing intermediate container 6f928ff5ec97
 ---> 2c51f4a80903
Step 4/13 : COPY main.go .
 ---> 919077d83e2b
Step 5/13 : RUN go build main.go
 ---> Running in 5959c30f5c10
Removing intermediate container 5959c30f5c10
 ---> cbe90ddb11ed
Step 6/13 : FROM busybox:ubuntu-14.04
ubuntu-14.04: Pulling from library/busybox
a3ed95caeb02: Pull complete
300273678d06: Pull complete
Digest: sha256:7d3ce4e482101f0c484602dd6687c826bb8bef6295739088c58e84245845912e
Status: Downloaded newer image for busybox:ubuntu-14.04
 ---> d16744963217
Step 7/13 : COPY --from=builder /go//main /app/guestbook
 ---> 67c829eb8d1b
Step 8/13 : ADD public/index.html /app/public/index.html
 ---> e71b4653ca17
Step 9/13 : ADD public/script.js /app/public/script.js
 ---> 1e3e52db271f
Step 10/13 : ADD public/style.css /app/public/style.css
 ---> b5b7fcfa50ca
Step 11/13 : WORKDIR /app
Removing intermediate container b982fa510f1c
 ---> 15947fedd20a
Step 12/13 : CMD ["./guestbook"]
 ---> Running in 156afadc0459
Removing intermediate container 156afadc0459
 ---> 0205392b8cbf
Step 13/13 : EXPOSE 3000
 ---> Running in 448df38be767
Removing intermediate container 448df38be767
 ---> 9708eb5366ec
Successfully built 9708eb5366ec
Successfully tagged guestbook:v1
```

Note that the image was given the tag v1. You should not use the latest tag because this will make Kubernetes try to pull the image from a public registry. We want it to use the locally stored image.

### Modifying the application

Let's make a simple change to the application and redeploy it. Open the file `public/script.js` in `vi` or your favorite editor. Modify the `handleSubmission` function so that it has an additional statement to append the date to the entry value, as shown below.

```js
  var handleSubmission = function(e) {
    e.preventDefault();
    var entryValue = entryContentElement.val()
    if (entryValue.length > 0) {
      entryValue += " " + new Date();     // ADD THIS LINE
      entriesElement.append("<p>...</p>");
      $.getJSON("rpush/guestbook/" + entryValue, appendGuestbookEntries);
	  entryContentElement.val("")
    }
    return false;
  }
```

Now rebuild the docker image and assign it a new tag.

```bash
$ docker build -t guestbook:v1.1 .
```

Congratulations you now have two versions of the application on your
local registry. Now lets spin up a local kubernetes cluster to verify
that it works as expected.


### Setting up a local multi-node cluster using kubeadm-dind-cluster

More advanced application development may require a multi-node cluster. We are going to use the project [kubernetes-sigs/kubeadm-dind-cluster](https://github.com/kubernetes-sigs/kubeadm-dind-cluster) to set up a multi-node Kubernetes cluster. It uses "Docker in Docker" to simulate multiple Kubernetes nodes in a single machine environment.

Do the following to set up and verify you have access to your `dind` cluster.

```bash
$ wget https://github.com/kubernetes-sigs/kubeadm-dind-cluster/releases/download/v0.1.0/dind-cluster-v1.13.sh
$ chmod +x dind-cluster-v1.13.sh

$ # start the cluster
$ ./dind-cluster-v1.13.sh up

$ # add kubectl directory to PATH
$ export PATH="$HOME/.kubeadm-dind-cluster:$PATH"

$ kubectl get nodes
NAME          STATUS    ROLES     AGE       VERSION
kube-master   Ready     master    4m        v1.13.0
kube-node-1   Ready     <none>    2m        v1.13.0
kube-node-2   Ready     <none>    2m        v1.13.0

$ # k8s dashboard available at http://localhost:8080/api/v1/namespaces/kube-system/services/kubernetes-dashboard:/proxy

$ # restart the cluster, this should happen much quicker than initial startup
$ ./dind-cluster-v1.13.sh up

$ # stop the cluster
$ ./dind-cluster-v1.13.sh down

$ # remove DIND containers and volumes
$ ./dind-cluster-v1.13.sh clean
```

If all of these passed, then spin up your cluster again with:

```bash
$ # start the cluster
$ ./dind-cluster-v1.13.sh up
```

Now lets push the first version of your container to your local kubernetes cluster.

```bpash
$ kubectl run guestbook --image=guestbook:v1
deployment.apps/guestbook created
```

We can use kubectl to verify that Kubernetes created a pod containing our container and that it's running.

```bash
$ kubectl get pods
NAME                         READY     STATUS    RESTARTS   AGE
guestbook-6cd549c68f-d6qvw   1/1       Running   0          1m
```

### Accessing the local running application

The guestbook application listens on port `3000` inside the pod. In order to make the application externally accessible we need to create a Kubernetes service of type `NodePort` for it. Kubernetes will allocate a port in the range `30000-32767` and the node will proxy that port to the pod's target port.

```bash
$ kubectl expose deployment guestbook --type=NodePort --port=3000
service/guestbook exposed
```

In order to access the service, we need to know the IP address of one of the worker nodes and the node port number that Kubernetes assigned to the service.

Get the IP address as follows:

```bash
$ docker exec -it kube-node-1 ip addr show eth0
15: eth0@if16: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:12:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.18.0.3/16 brd 172.18.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

This command displays the eth0 interface in the `kube-node-1` container. The container's IP address appears following inet and in this case is `172.18.0.3`.

Get the node port number as follows:

```
$ kubectl describe services/guestbook
Name:                     guestbook
Namespace:                default
Labels:                   run=guestbook
Annotations:              <none>
Selector:                 run=guestbook
Type:                     NodePort
IP:                       10.100.222.15
Port:                     <unset>  3000/TCP
TargetPort:               3000/TCP
NodePort:                 <unset>  32345/TCP
Endpoints:                10.244.2.3:3000
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

In this case the `NodePort` is 32345.

So in this case the application can be accessed from a browser running on the Linux host using the URL http://172.18.0.3:32345/. Note that although we are using the IP address of kube-node-1, the application does not need to be running on that node for this URL to work. Each node proxies the NodePort into the Service.

### Scaling the number of pods

Of course the reason for having a cluster is to increase capacity and improve availability by placing copies of an application on multiple nodes. Kubernetes calls these copies "replicas". Let's tell Kubernetes that we want two replicas of the application.

```bash
$ kubectl scale --replicas=2 deployment guestbook
deployment.extensions "guestbook" scaled
```

Kubernetes works in the background to start an additional pod to make the total number of pods equal to 2. You can check the status of this by running the kubectl get deployments command.

```bash
$ kubectl get deployments
NAME        DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
guestbook   2         2         2            2           10m
```

You can also see the status of the pods and which nodes they are running on as follows.

```bash
$ kubectl get pods -o wide
NAME                        READY     STATUS    RESTARTS   AGE       IP           NODE
guestbook-5986549d9-8dnhb   1/1       Running   0          7s        10.244.3.5   kube-node-2
guestbook-5986549d9-qtnmg   1/1       Running   0          7s        10.244.2.5   kube-node-1
```

Once both pods are running, try the following exercise. Open the application in a browser, enter a few messages, and close the browser. Repeat again a few times. You will see the display of previous guest messages changes because the browser is connecting to one node or the other and the messages are kept in memory. (You have to close the browser and re-open it to force a new connection, otherwise it will remain connected to the same node.)


<a name="namespaces"></a>
## Using namespaces to isolate your environments
<a name="namespaces"></a>

Taken directly from the [kubernetes](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) documentation:

> Kubernetes supports multiple virtual clusters backed by the same physical cluster. These virtual clusters are called namespaces.
>Namespaces are intended for use in environments with many users spread across multiple teams, or projects. For clusters with a few to tens of users, you should not need to create or think about namespaces at all. Start using namespaces when you need the features they provide.
> Namespaces provide a scope for names. Names of resources need to be unique within a namespace, but not across namespaces.
> Namespaces are a way to divide cluster resources between multiple users (via resource quota).

You can view your `name space`s via the following command:

```bash
kubectl get namespaces
```

By default you interface directly with the `default` namespace.

If you would like to run something in a new namespace (temporally) you can run something like the following:

```bash
kubectl --namespace=<insert-namespace-name-here> run nginx --image=nginx
kubectl --namespace=<insert-namespace-name-here> get pods
```

Then if you run the `get pods` you should see that you won't see the `nginx` pod.

```bash
kubectl get pods
```

Now if you want to _permanently_ change the names space you can run the following:

```bash
kubectl config set-context $(kubectl config current-context) --namespace=<insert-namespace-name-here>
# Validate it
kubectl config view | grep namespace:
```

### Creating Dev and Prod Namespaces

Lets go ahead and create two name spaces to emulate having a Dev and Prod environments on your cluster.

Lets create `dev.json`. Take the following a write it out as a file.

```json
{
  "kind": "Namespace",
  "apiVersion": "v1",
  "metadata": {
    "name": "dev",
    "labels": {
      "name": "dev"
    }
  }
}
```

Now run the following to create the namespace.

```bash
kubectl create -f dev.json
```

Now create a `prod.json` for our production environment.

```json
{
  "kind": "Namespace",
  "apiVersion": "v1",
  "metadata": {
    "name": "prod",
    "labels": {
      "name": "prod"
    }
  }
}
```

Now run the following to create the namespace.

```bash
kubectl create -f prod.json
```

Now lets verify that we have our namespaces:

```
kubectl get namespaces --show-labels
NAME          STATUS    AGE       LABELS
default       Active    32m       <none>
dev           Active    29s       name=dev
prod          Active    23s       name=prod
```

Now just like above, lets get an `nginx` running in our `dev` namespace.

```bash
kubectl --namespace=dev run nginx --image=nginx
```

Now lets run the `httpd` container in our `prod` namespace.

```bash
kubectl --namespace=prod run httpd --image=httpd
```

Now if you run the following you'll see different pods in different name spaces:

```bash
kubectl get pods
kubectl --namespace=dev get pods
kubectl --namespace=prod get pods
```

Success! Now you know how to create name spaces, you can slice and dice your kubernetes cluster
to your hearts content. Leveraging labels here you can attach specific name spaces to specific
nodes, or hardware, you can do some awesome things customizing your kubernetes cluster this way.

Check out the documentation [here](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
for some of the more powerful things you can do.

<a name="ingress"></a>
## Creating an ingress rule for your application
<a name="ingress"></a>

I searched high and low for a good example. Turns out [this site](https://medium.com/@cashisclay/kubernetes-ingress-82aa960f658e) has created probably the best example. There is no point in recreating
it, so go ahead and click that link and thank you [Jay Gorrell](https://medium.com/@cashisclay) for
helping out our community.

## Final Thoughts

If you have successfully gotten through our 101 and now this 201 kubernetes workshops believe
it or not you are extremely well versed in the practitioner level understanding of kubernetes.
You have the lions share of how to interface with kubernetes now, and congratulations!

Now go off and play with you kubernetes knowledge and keep your eye out for new things that get
released in the ecosystem!
